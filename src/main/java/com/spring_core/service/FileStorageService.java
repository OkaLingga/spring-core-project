package com.spring_core.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.spring_core.entity.FileStorageProperties;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

@Service
public class FileStorageService {
	private final Path fileStorageLocation;
        
    
    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) throws Exception {
    	String os = System.getProperty("os.name").toLowerCase();
		String path = "";
		if (os.contains("win")) {
			//Ambil setting folder Windows dr app.prop
            path = fileStorageProperties.getUploadDirWindows();
        } else if (os.contains("nix") || os.contains("nux") || os.contains("aix")) {
        	//Folder linux System.getProperty("user.home")+File.separator hasilnya /home/user/{kemudian ambil dr app.prop}
        	path = System.getProperty("user.home")+File.separator+fileStorageProperties.getUploadDirLinux();
        }
        this.fileStorageLocation = Paths.get(path)
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }
    
	public Path getFileStorageLocation() {
		return fileStorageLocation;
	}
	
	public void makeDirDokumen() {
		Path path = this.fileStorageLocation.resolve(Paths.get("dokumen")).normalize();
        try {
            Files.createDirectories(path);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
	}
	
	public void makeDirPegawai(String idpegawai, String jenis) {
		Path path = this.fileStorageLocation.resolve(Paths.get("pegawai", idpegawai, jenis)).normalize();
        try {
            Files.createDirectories(path);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
	}
	
	public void makeDirPerencanaan(String tahun, String kdsatker) {
		Path path = this.fileStorageLocation.resolve(Paths.get("perencanaan", kdsatker, tahun)).normalize();
        try {
            Files.createDirectories(path);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
	}

    public String storeFileDokumen(MultipartFile file, String iddokumen) {
        String fileExtension=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
        String fileName = StringUtils.cleanPath(iddokumen+"."+fileExtension).toLowerCase();

        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            makeDirDokumen();
            Path path = this.fileStorageLocation.resolve(Paths.get("dokumen")).normalize();
            Path targetLocation = path.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
    
    public String storeFilePegawai(MultipartFile file, String idpegawai, int z) {
    	String type="";
    	int no = z;
		if(z==1) {
			type = "surat_pernyataan";
		}else if(z==2) {
			no = z-4;
			type = "surat_keterangan";
		}else {
			no=1;
			type = "surat_usulan";
		}
        String fileExtension=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
        //String fileName = StringUtils.cleanPath(type+"_"+String.valueOf(no)+"."+fileExtension).toLowerCase();
        String fileName = StringUtils.cleanPath(type+"."+fileExtension).toLowerCase();
        
        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            makeDirPegawai(idpegawai, "surat");
            Path path = this.fileStorageLocation.resolve(Paths.get("pegawai", idpegawai, "surat")).normalize();
            Path targetLocation = path.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
    
    public String storeFileCv(MultipartFile file, String idpegawai, int z, String dokumen) {
        String fileExtension=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
        String fileName = StringUtils.cleanPath(dokumen+"_"+String.valueOf(z)+"."+fileExtension).toLowerCase();
        
        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            makeDirPegawai(idpegawai, dokumen);
            Path path = this.fileStorageLocation.resolve(Paths.get("pegawai", idpegawai, dokumen)).normalize();
            Path targetLocation = path.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
    
    public String storeFileRencana(MultipartFile file, String tahun, String kdsatker, int z) {
        String fileExtension=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
        String dokumen="";
        if(z==1) {
        	dokumen = "1.penetapan_formasi";
        }else if(z==2) {
        	dokumen = "2.rekomendasi_penetapan_formasi";
        }else if(z==3) {
        	dokumen = "3.persetujuan_penetapan_formasi";
        }
        String fileName = StringUtils.cleanPath(dokumen+"."+fileExtension).toLowerCase();
        
        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            makeDirPerencanaan(tahun, kdsatker);
            Path path = this.fileStorageLocation.resolve(Paths.get("perencanaan", kdsatker, tahun)).normalize();
            Path targetLocation = path.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String fileName) throws UnsupportedEncodingException {
        try {
        	String file = URLDecoder.decode(fileName, "UTF-8");
        	String url = file.replaceAll("-#-","/");
            Path filePath = this.fileStorageLocation.resolve(Paths.get(url)).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }
    
    public String getFileName(String file) {
    	Path path = this.fileStorageLocation.resolve(Paths.get(file)).normalize();
    	return path.getFileName().toString();
    }
    
    public Map<String, Object> cekFolderRoot() {
    	String os = System.getProperty("os.name");
    	
        Path p=Paths.get(this.fileStorageLocation.toString());
    	
        String pesan="OS : "+os+"<br/>Folder Root "+p+"<br/>";
        
        Map<String, Object> hasil = new HashMap<>();
        
    	if(!Files.isReadable(p) && !Files.isWritable(p)){
            pesan = pesan+"Tidak Dapat Read & Write";
            hasil.put("pesan",pesan);
            hasil.put("cek", false);
    	}else if(!Files.isWritable(p)) {
            pesan = pesan+"Tidak Dapat Write";
            hasil.put("pesan",pesan);
            hasil.put("cek", false);
    	}else if(!Files.isReadable(p)){
            pesan = pesan+"Tidak Dapat Read";
            hasil.put("pesan",pesan);
            hasil.put("cek", false);
    	}else {
            pesan = pesan+"OK";
            hasil.put("cek", true);
            hasil.put("pesan",pesan);
    	}
    	return hasil;
    }
    
    public Map<String, Object> deleteFile(String fileName) throws UnsupportedEncodingException {
    	Map<String, Object> hasil = new HashMap<>();
    	String file = URLDecoder.decode(fileName, "UTF-8");
    	String[] url = file.split("-#-");
        Path filePath = this.fileStorageLocation.resolve(Paths.get(url[0], url[1], url[2], url[3])).normalize();
        try {
        	if(Files.deleteIfExists(filePath)) {
        		hasil.put("pesan", "File Berhasil di hapus");
                hasil.put("cek", true);
        	} else {
        		hasil.put("pesan", "File tidak ada");
                hasil.put("cek", false);
        	}
		} catch (IOException e) {
			hasil.put("pesan", "File gagal di hapus");
            hasil.put("cek", false);
		}
    	return hasil;
    }
    
    public void deleteFileDokumen(String folder) throws UnsupportedEncodingException {
    	String file = URLDecoder.decode(folder, "UTF-8");
    	String[] url = file.split("-#-");
    	File filePath = this.fileStorageLocation.resolve(Paths.get(url[0], url[1])).normalize().toFile();
    	FileSystemUtils.deleteRecursively(filePath);
    }
    
    public void deleteFilePegawai(String idpegawai){
    	File filePath = this.fileStorageLocation.resolve(Paths.get("pegawai", idpegawai)).normalize().toFile();
    	FileSystemUtils.deleteRecursively(filePath);
    }
    
    public void deleteFilePerencanaan(String kdsatker, String tahun){
    	File filePath = this.fileStorageLocation.resolve(Paths.get("perencanaan", kdsatker, tahun)).normalize().toFile();
    	FileSystemUtils.deleteRecursively(filePath);
    }
    
}
