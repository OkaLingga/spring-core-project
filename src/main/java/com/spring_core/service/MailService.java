    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring_core.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.spring_core.dao.DefaultDao;
import com.spring_core.entity.Default;


@Service
public class MailService {

	/*
	 * @Value("${spring.datasource.url}") private String Url;
	 * 
	 * @Value("${spring.datasource.username}") private String username;
	 * 
	 * @Value("${spring.datasource.password}") private String password;
	 */
	
	@Autowired
	private DefaultDao defaultDao;
   
   public void SendMail(String send,String from, String subject, String message) throws SQLException
   {
       
       message = "DJPK - KEMENTERIAN KEUANGAN R.I \r\n"+message;
       Default d = new Default();
       d = defaultDao.findOne(Long.valueOf(1));
       
       String mailhost = d.getHost_email();
       String mailuser = d.getUser_email();
       String mailpass = d.getPass_email();
       Integer mailport = d.getPort_email();
       
       Properties mailProps = new Properties();
       mailProps.put("mail.smtp.starttls.enable", "true");
       
       from = "no-reply@kemenkeu.go.id";
       SimpleMailMessage mail = new SimpleMailMessage();
       mail.setTo(send);
       mail.setFrom(from);
       mail.setSubject(subject);
       mail.setText(message);
       JavaMailSenderImpl sender = new JavaMailSenderImpl();
       sender.setJavaMailProperties(mailProps);
       
       sender.setProtocol("smtp");
       sender.setHost(mailhost);
       sender.setPort(mailport);       
       sender.setUsername(mailuser);
       sender.setPassword(mailpass);
       sender.send(mail);
   }
}
