package com.spring_core.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring_core.dao.LevelDao;
import com.spring_core.dao.PemdaDao;
import com.spring_core.dao.UserDao;
import com.spring_core.entity.User;

@Controller
public class UserController {
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	LevelDao levelDao;
	
	@Autowired
	PemdaDao pemdaDao;
	
	@RequestMapping("/user")
	public String user(Model model) {
		model.addAttribute("level", levelDao.findAll());
		model.addAttribute("satker", pemdaDao.findAll());
		return "master/user";
	}
	
	@GetMapping("/list-user")
    public @ResponseBody Map<String, Object> roles() {
    	List<User> list = (List<User>) userDao.findAll();
        Map<String, Object> hasil = new HashMap<>();
        hasil.put("content", list);
        return hasil;
    }
	
	@PostMapping(path = "/save-user", consumes = "application/json", produces = "application/json")
	@ResponseBody
	public void saveRole(@RequestBody User entity) {
		BCryptPasswordEncoder b = new BCryptPasswordEncoder();
		entity.setPassword(b.encode(entity.getPassword()));
		userDao.save(entity);
	}
	
	@GetMapping("/list-user/{id}")
    public @ResponseBody Map<String, Object> getUser(@PathVariable("id") Integer id) {
    	User list = userDao.findOne(id);
        Map<String, Object> hasil = new HashMap<>();
        hasil.put("content", list);
        return hasil;
    }
}
