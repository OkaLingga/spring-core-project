package com.spring_core.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MenuController {
	
	@RequestMapping("/login")
	public String index() {
		return "login";
	}
	
	@RequestMapping("default")
    public String defaultAfterLogin(HttpServletRequest request) {
        return "redirect:/home";
    }
	
	@RequestMapping("/home")
	public String home() {
		return "home";
	}
	
}