package com.spring_core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name ="levelEntity")
@Table(name ="tlevel")
public class Level {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false,length = 11)
    private Long idleveluser;
	
	@Column(nullable = false)
    private String nmleveluser;

	public Long getIdleveluser() {
		return idleveluser;
	}

	public void setIdleveluser(Long idleveluser) {
		this.idleveluser = idleveluser;
	}

	public String getNmleveluser() {
		return nmleveluser;
	}

	public void setNmleveluser(String nmleveluser) {
		this.nmleveluser = nmleveluser;
	}
}
