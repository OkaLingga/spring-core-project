package com.spring_core.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {
	private String uploadDirWindows;
	private String uploadDirLinux;
	
	public String getUploadDirWindows() {
		return uploadDirWindows;
	}
	public void setUploadDirWindows(String uploadDirWindows) {
		this.uploadDirWindows = uploadDirWindows;
	}
	public String getUploadDirLinux() {
		return uploadDirLinux;
	}
	public void setUploadDirLinux(String uploadDirLinux) {
		this.uploadDirLinux = uploadDirLinux;
	}
	
	
}
