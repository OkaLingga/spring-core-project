package com.spring_core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity(name = "defaultEntity")
@Table(name = "tdefault")
public class Default implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, length = 11)
    private Long id_tdfault;
	
	@Column(nullable = false, length = 50)
    private String user_email;
	
	@Column(nullable = false, length = 50)
    private String pass_email;
	
	@Column(nullable = false, length = 11)
    private Integer port_email;
	
	@Column(nullable = false, length = 50)
    private String host_email;
	
	@Column(nullable = false, length = 100)
    private String folder_upload;

	public Long getId_tdfault() {
		return id_tdfault;
	}

	public void setId_tdfault(Long id_tdfault) {
		this.id_tdfault = id_tdfault;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getPass_email() {
		return pass_email;
	}

	public void setPass_email(String pass_email) {
		this.pass_email = pass_email;
	}

	public Integer getPort_email() {
		return port_email;
	}

	public void setPort_email(Integer port_email) {
		this.port_email = port_email;
	}

	public String getHost_email() {
		return host_email;
	}

	public void setHost_email(String host_email) {
		this.host_email = host_email;
	}

	public String getFolder_upload() {
		return folder_upload;
	}

	public void setFolder_upload(String folder_upload) {
		this.folder_upload = folder_upload;
	}
}
