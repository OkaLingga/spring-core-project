package com.spring_core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name ="pemdaEntity")
@Table(name ="tpemda")
public class Pemda {
	@Id
    @Column(nullable = false,length = 6)
    private String kdsatker;
	
	@Column(nullable = true,length = 2)
    private String kdprov;
	
	@Column(nullable = true,length = 2)
    private String 	kdpemda;
	
	@Column(nullable = true,length = 50)
    private String 	urpemda;

	public String getKdsatker() {
		return kdsatker;
	}

	public void setKdsatker(String kdsatker) {
		this.kdsatker = kdsatker;
	}

	public String getKdprov() {
		return kdprov;
	}

	public void setKdprov(String kdprov) {
		this.kdprov = kdprov;
	}

	public String getKdpemda() {
		return kdpemda;
	}

	public void setKdpemda(String kdpemda) {
		this.kdpemda = kdpemda;
	}

	public String getUrpemda() {
		return urpemda;
	}

	public void setUrpemda(String urpemda) {
		this.urpemda = urpemda;
	}
	
}
