package com.spring_core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name ="userEntity")
@Table(name ="tuser")
public class User {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false,length = 11)
    private Integer id_user;
	
	@Column(nullable = false, length = 50)
    private String 	username;
	
	@Column(nullable = false,length = 150)
    private String full_name;
	
	@Column(nullable = false,length = 100)
    private String password;
	
	@Column(nullable = false,length = 11)
    private Integer	idlevel;
	
	@Column(nullable = true,length = 1)
    private Integer	aktif;
	
	@Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date tgcreate;
	
	@Column(nullable = false)
    @JsonFormat(pattern="yyyy-mm-dd HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date tgupdate;

    @Column(nullable = false)
    @JsonFormat(pattern="yyyy-mm-dd HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date tgaktif;
    
    @Column(length = 6, columnDefinition = "LONGVARBINARY")
    @NotFound(action = NotFoundAction.IGNORE)
    private String kdsatker;
    
    @Column(nullable = true,length = 50)
    private String notelp;
    
    @Column(nullable = true,length = 50)
    private String nohp;
    
    @Column(nullable = true,length = 50)
    private String 	jabatan;

    
	public User() {
	}

	public User(Integer id_user, String username, String full_name, String password, Integer idlevel, Integer aktif,
			Date tgcreate, Date tgupdate, Date tgaktif, String kdsatker, String notelp, String nohp, String jabatan) {
		super();
		this.id_user = id_user;
		this.username = username;
		this.full_name = full_name;
		this.password = password;
		this.idlevel = idlevel;
		this.aktif = aktif;
		this.tgcreate = tgcreate;
		this.tgupdate = tgupdate;
		this.tgaktif = tgaktif;
		this.kdsatker = kdsatker;
		this.notelp = notelp;
		this.nohp = nohp;
		this.jabatan = jabatan;
	}

	public Integer getId_user() {
		return id_user;
	}

	public void setId_user(Integer id_user) {
		this.id_user = id_user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getIdlevel() {
		return idlevel;
	}

	public void setIdlevel(Integer idlevel) {
		this.idlevel = idlevel;
	}

	public Integer getAktif() {
		return aktif;
	}

	public void setAktif(Integer aktif) {
		this.aktif = aktif;
	}

	public Date getTgcreate() {
		return tgcreate;
	}

	public void setTgcreate(Date tgcreate) {
		this.tgcreate = tgcreate;
	}

	public Date getTgupdate() {
		return tgupdate;
	}

	public void setTgupdate(Date tgupdate) {
		this.tgupdate = tgupdate;
	}

	public Date getTgaktif() {
		return tgaktif;
	}

	public void setTgaktif(Date tgaktif) {
		this.tgaktif = tgaktif;
	}

	public String getKdsatker() {
		return kdsatker;
	}

	public void setKdsatker(String kdsatker) {
		this.kdsatker = kdsatker;
	}

	public String getNotelp() {
		return notelp;
	}

	public void setNotelp(String notelp) {
		this.notelp = notelp;
	}

	public String getNohp() {
		return nohp;
	}

	public void setNohp(String nohp) {
		this.nohp = nohp;
	}

	public String getJabatan() {
		return jabatan;
	}

	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}
}
