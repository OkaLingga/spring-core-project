package com.spring_core.dao;

import org.springframework.data.repository.CrudRepository;

import com.spring_core.entity.Default;



public interface DefaultDao extends CrudRepository<Default, Long>{

}
