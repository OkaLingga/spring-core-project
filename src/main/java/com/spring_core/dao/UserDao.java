package com.spring_core.dao;

import org.springframework.data.repository.CrudRepository;

import com.spring_core.entity.User;

public interface UserDao extends CrudRepository<User, Integer>{
}
