package com.spring_core.dao;

import org.springframework.data.repository.CrudRepository;

import com.spring_core.entity.Pemda;

public interface PemdaDao extends CrudRepository<Pemda, String>{
	
	
}
