package com.spring_core.dao;

import org.springframework.data.repository.CrudRepository;

import com.spring_core.entity.Level;

public interface LevelDao extends CrudRepository<Level, Long>{
	
	
}
