-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2020 at 05:30 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spring_core`
--

-- --------------------------------------------------------

--
-- Table structure for table `tlevel`
--

CREATE TABLE `tlevel` (
  `idleveluser` int(11) NOT NULL COMMENT 'ID tleveluser',
  `nmleveluser` varchar(50) DEFAULT NULL COMMENT 'Nama level user',
  `aktif` tinyint(4) DEFAULT NULL COMMENT 'aktif tidak'
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

--
-- Dumping data for table `tlevel`
--

INSERT INTO `tlevel` (`idleveluser`, `nmleveluser`, `aktif`) VALUES
(1, 'Admin', 1),
(2, 'User', 1),
(3, 'Guest', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tpemda`
--

CREATE TABLE `tpemda` (
  `kdsatker` char(6) NOT NULL COMMENT 'kode satker pemda',
  `kdprov` char(2) CHARACTER SET utf8 DEFAULT NULL COMMENT 'kode provinsi',
  `kdpemda` char(2) CHARACTER SET utf8 DEFAULT NULL COMMENT 'kode kab/kota',
  `urpemda` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'uraian prov/kab/kota'
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

--
-- Dumping data for table `tpemda`
--

INSERT INTO `tpemda` (`kdsatker`, `kdprov`, `kdpemda`, `urpemda`) VALUES
('928282', '20', '13', 'Kab. Luwu Timur'),
('941901', '34', '04', 'Kota Tarakan'),
('941915', '02', '07', 'Kab. Mandailing Natal'),
('941922', '02', '13', 'Kab. Toba Samosir'),
('955383', '33', '00', 'Provinsi Sulawesi Barat'),
('963302', '10', '26', 'Kab. Bandung Barat'),
('963311', '01', '22', 'Kab. Pidie Jaya'),
('963327', '01', '23', 'Kota Subulussalam'),
('963333', '02', '26', 'Kab. Batu Bara'),
('963342', '06', '15', 'Kab. Empat Lawang'),
('963358', '14', '13', 'Kab. Kayong Utara'),
('963364', '21', '11', 'Kab. Konawe Utara'),
('963370', '21', '12', 'Kab. Buton Utara'),
('963389', '18', '10', 'Kab. Kep. Siau Tagulandang Biaro'),
('963395', '18', '11', 'Kota Kotamobagu'),
('963409', '18', '12', 'Kab. Bolaang Mongondow Utara'),
('963415', '18', '13', 'Kab. Minahasa Tenggara'),
('963421', '30', '06', 'Kab. Gorontalo Utara'),
('963430', '24', '17', 'Kab. Nagekeo'),
('963446', '24', '18', 'Kab. Sumba Barat Daya'),
('963452', '24', '19', 'Kab. Sumba Tengah'),
('963461', '26', '21', 'Kab. Mamberamo Raya'),
('975013', '02', '27', 'Kab. Padang Lawas'),
('975022', '18', '14', 'Kab. Bolaang Mongondow Timur'),
('975163', '02', '28', 'Kab. Padang Lawas Utara'),
('975172', '19', '11', 'Kab. Sigi'),
('975311', '08', '11', 'Kab. Pesawaran'),
('975327', '20', '25', 'Kab. Toraja Utara'),
('975461', '28', '07', 'Kota Serang'),
('975477', '23', '10', 'Kab. Lombok Utara'),
('975616', '14', '14', 'Kab. Kubu Raya'),
('975622', '25', '11', 'Kab. Buru Selatan'),
('975766', '34', '05', 'Kab. Tana Tidung'),
('975772', '25', '10', 'Kab. Maluku Barat Daya'),
('975911', '24', '20', 'Kab. Manggarai Timur'),
('976061', '25', '09', 'Kota Tual'),
('976219', '26', '22', 'Kab. Mamberamo Tengah'),
('976369', '26', '23', 'Kab. Yalimo'),
('976514', '26', '24', 'Kab. Lanny Jaya'),
('976664', '26', '25', 'Kab. Nduga'),
('976812', '26', '27', 'Kab. Puncak'),
('976962', '26', '26', 'Kab. Dogiyai'),
('977117', '02', '30', 'Kab. Labuhan Batu Utara'),
('977267', '02', '29', 'Kab. Labuhan Batu Selatan'),
('977412', '31', '02', 'Kab. Kepulauan Anambas'),
('977562', '05', '11', 'Kota Sungai Penuh'),
('977710', '07', '10', 'Kab. Bengkulu Tengah'),
('977860', '18', '15', 'Kab. Bolaang Mongondow Selatan'),
('978397', '02', '32', 'Kab. Nias Barat'),
('978401', '02', '33', 'Kota Gunungsitoli'),
('978417', '04', '12', 'Kab. Kepulauan Meranti'),
('978423', '08', '12', 'Kab. Pringsewu'),
('978432', '08', '13', 'Kab. Mesuji'),
('978448', '08', '14', 'Kab. Tulang Bawang Barat'),
('978454', '28', '08', 'Kota Tangerang Selatan'),
('978536', '02', '31', 'Kab. Nias Utara'),
('978542', '24', '21', 'Kab. Sabu Raijua'),
('978551', '27', '09', 'Kab. Pulau Morotai'),
('978567', '26', '28', 'Kab. Intan Jaya'),
('978573', '26', '29', 'Kab. Deiyai'),
('978582', '32', '10', 'Kab. Maybrat'),
('978598', '32', '11', 'Kab. Tambrauw'),
('980007', '01', '04', 'Kab. Aceh Singkil'),
('980011', '27', '00', 'Provinsi Maluku Utara'),
('980028', '27', '02', 'Kota Ternate'),
('980032', '10', '21', 'Kota Depok'),
('980049', '28', '05', 'Kota Cilegon'),
('980053', '01', '09', 'Kab. Bireuen'),
('980060', '03', '03', 'Kab. Kepulauan Mentawai'),
('980074', '04', '06', 'Kab. Pelalawan'),
('980081', '04', '08', 'Kab. Rokan Hulu'),
('980095', '04', '07', 'Kab. Rokan Hilir'),
('980100', '04', '09', 'Kab. Siak'),
('980117', '31', '03', 'Kab. Karimun'),
('980121', '31', '01', 'Kab. Natuna'),
('980138', '04', '05', 'Kab. Kuantan Singingi'),
('980142', '04', '10', 'Kota Dumai'),
('980163', '05', '06', 'Kab. Sarolangun'),
('980170', '05', '09', 'Kab. Tebo'),
('980184', '05', '05', 'Kab. Muaro Jambi'),
('980191', '08', '05', 'Kab. Lampung Timur'),
('980206', '08', '08', 'Kab. Way Kanan'),
('980210', '08', '10', 'Kota Metro'),
('980227', '14', '01', 'Kab. Bengkayang'),
('980231', '14', '02', 'Kab. Landak'),
('980248', '16', '10', 'Kota Banjarbaru'),
('980252', '34', '03', 'Kab. Nunukan'),
('980269', '34', '02', 'Kab. Malinau'),
('980273', '17', '03', 'Kab. Kutai Barat'),
('980280', '17', '04', 'Kab. Kutai Timur'),
('980294', '17', '07', 'Kota Bontang'),
('980302', '30', '01', 'Kab. Boalemo'),
('980316', '19', '03', 'Kab. Buol'),
('980320', '19', '06', 'Kab. Morowali'),
('980337', '19', '02', 'Kab. Banggai Kepulauan'),
('980341', '20', '09', 'Kab. Luwu Utara'),
('980358', '25', '01', 'Kab. Maluku Tenggara Barat'),
('980362', '25', '04', 'Kab. Buru'),
('980379', '24', '06', 'Kab. Lembata'),
('980383', '32', '04', 'Kota Sorong'),
('980522', '05', '08', 'Kab. Tanjung Jabung Timur'),
('987418', '02', '24', 'Kab. Serdang Berdagai'),
('987422', '25', '07', 'Kab. Seram Bagian Timur'),
('987439', '25', '06', 'Kab. Seram Bagian Barat'),
('987443', '21', '10', 'Kab. Kolaka Utara'),
('987450', '23', '09', 'Kab. Sumbawa Barat'),
('987464', '31', '06', 'Kab. Lingga'),
('987471', '19', '10', 'Kab. Tojo Una Una'),
('987485', '18', '09', 'Kab. Minahasa Utara'),
('987492', '14', '12', 'Kab. Melawi'),
('987507', '14', '11', 'Kab. Sekadau'),
('987511', '26', '20', 'Kab. Supiori'),
('987528', '02', '25', 'Kab. Samosir'),
('987532', '01', '21', 'Kab. Bener Meriah'),
('987549', '06', '13', 'Kab. OKU Timur'),
('987553', '06', '14', 'Kab. OKU Selatan'),
('987560', '06', '12', 'Kab. Ogan Ilir'),
('987574', '03', '18', 'Kab. Dharmasraya'),
('987581', '03', '19', 'Kab. Solok Selatan'),
('987595', '03', '17', 'Kab. Pasaman Barat'),
('987600', '07', '08', 'Kab. Lebong'),
('987617', '07', '09', 'Kab. Kepahiang'),
('987621', '25', '08', 'Kab. Kepulauan Aru'),
('987638', '21', '09', 'Kab. Wakatobi'),
('987642', '21', '08', 'Kab. Bombana'),
('990015', '01', '00', 'Provinsi Aceh'),
('990022', '01', '02', 'Kab. Aceh Besar'),
('990036', '01', '10', 'Kab. Pidie'),
('990040', '01', '08', 'Kab. Aceh Utara'),
('990057', '01', '07', 'Kab. Aceh Timur'),
('990061', '01', '03', 'Kab. Aceh Selatan'),
('990078', '01', '01', 'Kab. Aceh Barat'),
('990082', '01', '05', 'Kab. Aceh Tengah'),
('990099', '01', '06', 'Kab. Aceh Tenggara'),
('990104', '01', '12', 'Kota Banda Aceh'),
('990111', '01', '13', 'Kota Sabang'),
('990125', '02', '00', 'Provinsi Sumatera Utara'),
('990132', '02', '03', 'Kab. Deli Serdang'),
('990146', '02', '04', 'Kab. Tanah Karo'),
('990150', '02', '06', 'Kab. Langkat'),
('990167', '02', '11', 'Kab. Tapanuli Tengah'),
('990171', '02', '09', 'Kab. Simalungun'),
('990188', '02', '05', 'Kab. Labuhan Batu'),
('990192', '02', '02', 'Kab. Dairi'),
('990200', '02', '12', 'Kab. Tapanuli Utara'),
('990214', '02', '10', 'Kab. Tapanuli Selatan'),
('990221', '02', '01', 'Kab. Asahan'),
('990235', '02', '08', 'Kab. Nias'),
('990242', '02', '15', 'Kota Medan'),
('990256', '02', '19', 'Kota Tebing Tinggi'),
('990260', '02', '14', 'Kota Binjai'),
('990277', '02', '16', 'Kota Pematang Siantar'),
('990281', '02', '18', 'Kota Tanjung Balai'),
('990298', '02', '17', 'Kota Sibolga'),
('990303', '03', '00', 'Provinsi Sumatera Barat'),
('990310', '03', '02', 'Kab. Agam'),
('990324', '03', '05', 'Kab. Pasaman'),
('990331', '03', '01', 'Kab. Limapuluh Kota'),
('990345', '03', '08', 'Kab. Solok'),
('990352', '03', '04', 'Kab. Padang Pariaman'),
('990366', '03', '06', 'Kab. Pesisir Selatan'),
('990370', '03', '09', 'Kab. Tanah Datar'),
('990387', '03', '07', 'Kab. Sijunjung'),
('990391', '03', '10', 'Kota Bukit Tinggi'),
('990409', '03', '11', 'Kota Padang Panjang'),
('990413', '03', '15', 'Kota Solok'),
('990420', '03', '14', 'Kota Sawahlunto'),
('990434', '03', '12', 'Kota Padang'),
('990441', '03', '13', 'Kota Payakumbuh'),
('990455', '04', '00', 'Provinsi Riau'),
('990462', '04', '04', 'Kab. Kampar'),
('990476', '04', '01', 'Kab. Bengkalis'),
('990480', '31', '07', 'Kab. Bintan'),
('990497', '04', '03', 'Kab. Indragiri Hulu'),
('990502', '04', '02', 'Kab. Indragiri Hilir'),
('990519', '04', '11', 'Kota Pekanbaru'),
('990523', '31', '04', 'Kota Batam'),
('990530', '05', '00', 'Provinsi Jambi'),
('990544', '05', '01', 'Kab. Batanghari'),
('990551', '05', '07', 'Kab. Tanjung Jabung Barat'),
('990565', '05', '02', 'Kab. Bungo'),
('990572', '05', '04', 'Kab. Merangin'),
('990586', '05', '03', 'Kab. Kerinci'),
('990590', '05', '10', 'Kota Jambi'),
('990608', '06', '00', 'Provinsi Sumatera Selatan'),
('990612', '29', '02', 'Kab. Belitung'),
('990629', '29', '01', 'Kab. Bangka'),
('990633', '06', '02', 'Kab. Musi Banyuasin'),
('990640', '06', '06', 'Kab. Ogan Komering Ulu'),
('990654', '06', '04', 'Kab. Muara Enim'),
('990661', '06', '01', 'Kab. Lahat'),
('990675', '06', '03', 'Kab. Musi Rawas'),
('990682', '06', '05', 'Kab. Ogan Komering Ilir'),
('990696', '06', '07', 'Kota Palembang'),
('990701', '29', '03', 'Kota Pangkal Pinang'),
('990718', '07', '00', 'Provinsi Bengkulu'),
('990722', '07', '02', 'Kab. Bengkulu Utara'),
('990739', '07', '01', 'Kab. Bengkulu Selatan'),
('990743', '07', '03', 'Kab. Rejang Lebong'),
('990750', '07', '04', 'Kota Bengkulu'),
('990764', '08', '00', 'Provinsi Lampung'),
('990771', '08', '02', 'Kab. Lampung Selatan'),
('990785', '08', '03', 'Kab. Lampung Tengah'),
('990792', '08', '04', 'Kab. Lampung Utara'),
('990807', '08', '01', 'Kab. Lampung Barat'),
('990811', '08', '09', 'Kota Bandar Lampung'),
('990828', '09', '00', 'Provinsi DKI Jakarta'),
('990881', '10', '00', 'Provinsi Jawa Barat'),
('990895', '28', '03', 'Kab. Serang'),
('990900', '28', '02', 'Kab. Pandeglang'),
('990917', '28', '01', 'Kab. Lebak'),
('990921', '28', '04', 'Kab. Tangerang'),
('990938', '10', '03', 'Kab. Bogor'),
('990942', '10', '14', 'Kab. Sukabumi'),
('990959', '10', '05', 'Kab. Cianjur'),
('990963', '10', '02', 'Kab. Bekasi'),
('990970', '10', '09', 'Kab. Karawang'),
('990984', '10', '12', 'Kab. Purwakarta'),
('990991', '10', '13', 'Kab. Subang'),
('991003', '10', '01', 'Kab. Bandung'),
('991010', '10', '15', 'Kab. Sumedang'),
('991024', '10', '07', 'Kab. Garut'),
('991031', '10', '16', 'Kab. Tasikmalaya'),
('991045', '10', '04', 'Kab. Ciamis'),
('991052', '10', '06', 'Kab. Cirebon'),
('991066', '10', '10', 'Kab. Kuningan'),
('991070', '10', '08', 'Kab. Indramayu'),
('991087', '10', '11', 'Kab. Majalengka'),
('991091', '10', '17', 'Kota Bandung'),
('991109', '10', '19', 'Kota Bogor'),
('991113', '10', '22', 'Kota Sukabumi'),
('991120', '10', '20', 'Kota Cirebon'),
('991134', '28', '06', 'Kota Tangerang'),
('991141', '11', '00', 'Provinsi Jawa Tengah'),
('991155', '11', '23', 'Kab. Semarang'),
('991162', '11', '13', 'Kab. Kendal'),
('991176', '11', '08', 'Kab. Demak'),
('991180', '11', '09', 'Kab. Grobogan'),
('991197', '11', '18', 'Kab. Pekalongan'),
('991202', '11', '03', 'Kab. Batang'),
('991219', '11', '26', 'Kab. Tegal'),
('991223', '11', '06', 'Kab. Brebes'),
('991230', '11', '17', 'Kab. Pati'),
('991244', '11', '15', 'Kab. Kudus'),
('991251', '11', '19', 'Kab. Pemalang'),
('991265', '11', '10', 'Kab. Jepara'),
('991272', '11', '22', 'Kab. Rembang'),
('991286', '11', '04', 'Kab. Blora'),
('991290', '11', '02', 'Kab. Banyumas'),
('991308', '11', '07', 'Kab. Cilacap'),
('991312', '11', '20', 'Kab. Purbalingga'),
('991329', '11', '01', 'Kab. Banjarnegara'),
('991333', '11', '16', 'Kab. Magelang'),
('991340', '11', '27', 'Kab. Temanggung'),
('991354', '11', '29', 'Kab. Wonosobo'),
('991361', '11', '21', 'Kab. Purworejo'),
('991375', '11', '12', 'Kab. Kebumen'),
('991382', '11', '14', 'Kab. Klaten'),
('991396', '11', '05', 'Kab. Boyolali'),
('991401', '11', '24', 'Kab. Sragen'),
('991418', '11', '25', 'Kab. Sukoharjo'),
('991422', '11', '11', 'Kab. Karanganyar'),
('991439', '11', '28', 'Kab. Wonogiri'),
('991443', '11', '33', 'Kota Semarang'),
('991450', '11', '32', 'Kota Salatiga'),
('991464', '11', '31', 'Kota Pekalongan'),
('991471', '11', '35', 'Kota Tegal'),
('991485', '11', '30', 'Kota Magelang'),
('991492', '11', '34', 'Kota Surakarta'),
('991507', '12', '00', 'Provinsi DI Yogyakarta'),
('991511', '12', '01', 'Kab. Bantul'),
('991528', '12', '04', 'Kab. Sleman'),
('991532', '12', '02', 'Kab. Gunung Kidul'),
('991549', '12', '03', 'Kab. Kulon Progo'),
('991553', '12', '05', 'Kota Yogyakarta'),
('991560', '13', '00', 'Provinsi Jawa Timur'),
('991574', '13', '06', 'Kab. Gresik'),
('991581', '13', '15', 'Kab. Mojokerto'),
('991595', '13', '24', 'Kab. Sidoarjo'),
('991600', '13', '08', 'Kab. Jombang'),
('991617', '13', '23', 'Kab. Sampang'),
('991621', '13', '19', 'Kab. Pamekasan'),
('991638', '13', '26', 'Kab. Sumenep'),
('991642', '13', '01', 'Kab. Bangkalan'),
('991659', '13', '05', 'Kab. Bondowoso'),
('991663', '13', '25', 'Kab. Situbondo'),
('991670', '13', '02', 'Kab. Banyuwangi'),
('991684', '13', '07', 'Kab. Jember'),
('991691', '13', '14', 'Kab. Malang'),
('991706', '13', '20', 'Kab. Pasuruan'),
('991710', '13', '22', 'Kab. Probolinggo'),
('991727', '13', '11', 'Kab. Lumajang'),
('991731', '13', '09', 'Kab. Kediri'),
('991748', '13', '29', 'Kab. Tulungagung'),
('991752', '13', '16', 'Kab. Nganjuk'),
('991769', '13', '27', 'Kab. Trenggalek'),
('991773', '13', '03', 'Kab. Blitar'),
('991780', '13', '12', 'Kab. Madiun'),
('991794', '13', '17', 'Kab. Ngawi'),
('991802', '13', '13', 'Kab. Magetan'),
('991816', '13', '21', 'Kab. Ponorogo'),
('991820', '13', '18', 'Kab. Pacitan'),
('991837', '13', '04', 'Kab. Bojonegoro'),
('991841', '13', '28', 'Kab. Tuban'),
('991858', '13', '10', 'Kab. Lamongan'),
('991862', '13', '37', 'Kota Surabaya'),
('991879', '13', '34', 'Kota Mojokerto'),
('991883', '13', '33', 'Kota Malang'),
('991890', '13', '35', 'Kota Pasuruan'),
('991905', '13', '36', 'Kota Probolinggo'),
('991912', '13', '30', 'Kota Blitar'),
('991926', '13', '31', 'Kota Kediri'),
('991930', '13', '32', 'Kota Madiun'),
('991947', '14', '00', 'Provinsi Kalimantan Barat'),
('991951', '14', '06', 'Kab. Sambas'),
('991968', '14', '07', 'Kab. Sanggau'),
('991972', '14', '08', 'Kab. Sintang'),
('991989', '14', '05', 'Kab. Mempawah'),
('991993', '14', '03', 'Kab. Kapuas Hulu'),
('992008', '14', '04', 'Kab. Ketapang'),
('992012', '14', '09', 'Kota Pontianak'),
('992029', '15', '00', 'Provinsi Kalimantan Tengah'),
('992033', '15', '03', 'Kab. Kapuas'),
('992040', '15', '02', 'Kab. Barito Utara'),
('992054', '15', '01', 'Kab. Barito Selatan'),
('992061', '15', '05', 'Kab. Kotawaringin Timur'),
('992075', '15', '04', 'Kab. Kotawaringin Barat'),
('992082', '15', '06', 'Kota Palangkaraya'),
('992096', '17', '00', 'Provinsi Kalimantan Timur'),
('992101', '17', '02', 'Kab. Kutai Kartanegara'),
('992118', '17', '05', 'Kab. Paser'),
('992122', '34', '01', 'Kab. Bulungan'),
('992139', '17', '01', 'Kab. Berau'),
('992143', '17', '08', 'Kota Samarinda'),
('992150', '17', '06', 'Kota Balikpapan'),
('992164', '16', '00', 'Provinsi Kalimantan Selatan'),
('992171', '16', '01', 'Kab. Banjar'),
('992185', '16', '08', 'Kab. Tanah Laut'),
('992192', '16', '09', 'Kab. Tapin'),
('992207', '16', '03', 'Kab. Hulu Sungai Selatan'),
('992211', '16', '04', 'Kab. Hulu Sungai Tengah'),
('992228', '16', '02', 'Kab. Barito Kuala'),
('992232', '16', '07', 'Kab. Tabalong'),
('992249', '16', '06', 'Kab. Kotabaru'),
('992253', '16', '05', 'Kab. Hulu Sungai Utara'),
('992260', '16', '11', 'Kota Banjarmasin'),
('992274', '22', '00', 'Provinsi Bali'),
('992281', '22', '03', 'Kab. Buleleng'),
('992295', '22', '05', 'Kab. Jembrana'),
('992300', '22', '07', 'Kab. Klungkung'),
('992317', '22', '04', 'Kab. Gianyar'),
('992321', '22', '06', 'Kab. Karangasem'),
('992338', '22', '02', 'Kab. Bangli'),
('992342', '22', '01', 'Kab. Badung'),
('992359', '22', '08', 'Kab. Tabanan'),
('992363', '22', '09', 'Kota Denpasar'),
('992370', '23', '00', 'Provinsi Nusa Tenggara Barat'),
('992384', '23', '03', 'Kab. Lombok Barat'),
('992391', '23', '04', 'Kab. Lombok Tengah'),
('992406', '23', '05', 'Kab. Lombok Timur'),
('992410', '23', '01', 'Kab. Bima'),
('992427', '23', '06', 'Kab. Sumbawa'),
('992431', '23', '02', 'Kab. Dompu'),
('992448', '23', '07', 'Kota Mataram'),
('992452', '24', '00', 'Provinsi Nusa Tenggara Timur'),
('992469', '24', '05', 'Kab. Kupang'),
('992473', '24', '02', 'Kab. Belu'),
('992480', '24', '13', 'Kab. Timor Tengah Utara'),
('992494', '24', '12', 'Kab. Timor Tengah Selatan'),
('992502', '24', '01', 'Kab. Alor'),
('992516', '24', '09', 'Kab. Sikka'),
('992520', '24', '04', 'Kab. Flores Timur'),
('992537', '24', '03', 'Kab. Ende'),
('992541', '24', '08', 'Kab. Ngada'),
('992558', '24', '07', 'Kab. Manggarai'),
('992562', '24', '11', 'Kab. Sumba Timur'),
('992579', '24', '10', 'Kab. Sumba Barat'),
('992722', '20', '00', 'Provinsi Sulawesi Selatan'),
('992736', '20', '14', 'Kab. Pinrang'),
('992740', '20', '06', 'Kab. Gowa'),
('992757', '20', '22', 'Kab. Wajo'),
('992761', '33', '02', 'Kab. Mamuju'),
('992778', '20', '03', 'Kab. Bone'),
('992782', '20', '21', 'Kab. Tana Toraja'),
('992799', '20', '10', 'Kab. Maros'),
('992804', '33', '01', 'Kab. Majene'),
('992811', '20', '08', 'Kab. Luwu'),
('992825', '20', '15', 'Kab. Sinjai'),
('992832', '20', '04', 'Kab. Bulukumba'),
('992846', '20', '01', 'Kab. Bantaeng'),
('992850', '20', '07', 'Kab. Jeneponto'),
('992867', '20', '16', 'Kab. Kepulauan Selayar'),
('992871', '20', '20', 'Kab. Takalar'),
('992888', '20', '02', 'Kab. Barru'),
('992892', '20', '17', 'Kab. Sidenreng Rappang'),
('992900', '20', '11', 'Kab. Pangkajene Kepulauan'),
('992914', '20', '19', 'Kab. Soppeng'),
('992921', '33', '03', 'Kab. Polewali Mandar'),
('992935', '20', '05', 'Kab. Enrekang'),
('992942', '20', '24', 'Kota Makassar'),
('992956', '20', '23', 'Kota Pare-pare'),
('992960', '19', '00', 'Provinsi Sulawesi Tengah'),
('992977', '19', '07', 'Kab. Poso'),
('992981', '19', '05', 'Kab. Donggala'),
('992998', '19', '04', 'Kab. Toli-Toli'),
('993000', '19', '01', 'Kab. Banggai'),
('993017', '19', '08', 'Kota Palu'),
('993021', '18', '00', 'Provinsi Sulawesi Utara'),
('993038', '30', '02', 'Kab. Gorontalo'),
('993042', '18', '02', 'Kab. Minahasa'),
('993059', '18', '01', 'Kab. Bolaang Mongondow'),
('993063', '18', '03', 'Kab. Sangihe'),
('993070', '18', '05', 'Kota Manado'),
('993084', '30', '03', 'Kota Gorontalo'),
('993091', '18', '04', 'Kota Bitung'),
('993106', '21', '00', 'Provinsi Sulawesi Tenggara'),
('993110', '21', '02', 'Kab. Konawe'),
('993127', '21', '01', 'Kab. Buton'),
('993131', '21', '04', 'Kab. Muna'),
('993148', '21', '03', 'Kab. Kolaka'),
('993152', '25', '00', 'Provinsi Maluku'),
('993169', '25', '02', 'Kab. Maluku Tengah'),
('993173', '25', '03', 'Kab. Maluku Tenggara'),
('993180', '27', '03', 'Kab. Halmahera Barat'),
('993194', '27', '01', 'Kab. Halmahera Tengah'),
('993202', '25', '05', 'Kota Ambon'),
('993216', '26', '00', 'Provinsi Papua'),
('993220', '26', '02', 'Kab. Jayapura'),
('993237', '26', '01', 'Kab. Biak Numfor'),
('993241', '32', '02', 'Kab. Manokwari'),
('993258', '26', '09', 'Kab. Kepulauan Yapen'),
('993262', '32', '03', 'Kab. Sorong'),
('993279', '32', '01', 'Kab. Fak Fak'),
('993283', '26', '04', 'Kab. Merauke'),
('993290', '26', '03', 'Kab. Jayawijaya'),
('993305', '26', '06', 'Kab. Nabire'),
('993312', '26', '10', 'Kota Jayapura'),
('993326', '21', '05', 'Kota Kendari'),
('993330', '10', '18', 'Kota Bekasi'),
('993347', '08', '07', 'Kab. Tulang Bawang'),
('993351', '08', '06', 'Kab. Tanggamus'),
('993368', '24', '14', 'Kota Kupang'),
('993372', '01', '11', 'Kab. Simeulue'),
('993389', '26', '08', 'Kab. Puncak Jaya'),
('993393', '26', '07', 'Kab. Paniai'),
('993401', '26', '05', 'Kab. Mimika'),
('993809', '28', '00', 'Provinsi Banten'),
('993813', '29', '00', 'Provinsi Bangka Belitung'),
('993820', '30', '00', 'Provinsi Gorontalo'),
('994672', '01', '14', 'Kota Langsa'),
('994686', '01', '15', 'Kota Lhokseumawe'),
('994690', '02', '20', 'Kota Padang Sidempuan'),
('994708', '31', '05', 'Kota Tanjung Pinang'),
('994712', '06', '08', 'Kota Prabumulih'),
('994729', '06', '09', 'Kota Pagar Alam'),
('994733', '06', '10', 'Kota Lubuk Linggau'),
('994740', '10', '23', 'Kota Tasikmalaya'),
('994754', '10', '24', 'Kota Cimahi'),
('994761', '13', '38', 'Kota Batu'),
('994775', '14', '10', 'Kota Singkawang'),
('994782', '21', '06', 'Kota Bau-bau'),
('997412', '18', '06', 'Kab. Kepulauan Talaud'),
('997426', '01', '17', 'Kab. Aceh Barat Daya'),
('997430', '01', '16', 'Kab. Gayo Lues'),
('997447', '01', '18', 'Kab. Aceh Jaya'),
('997451', '01', '19', 'Kab. Nagan Raya'),
('997468', '01', '20', 'Kab. Aceh Tamiang'),
('997472', '15', '07', 'Kab. Katingan'),
('997489', '15', '08', 'Kab. Seruyan'),
('997493', '15', '09', 'Kab. Sukamara'),
('997501', '15', '10', 'Kab. Lamandau'),
('997515', '15', '11', 'Kab. Gunung Mas'),
('997522', '15', '12', 'Kab. Pulang Pisau'),
('997536', '15', '13', 'Kab. Murung Raya'),
('997540', '15', '14', 'Kab. Barito Timur'),
('997557', '06', '11', 'Kab. Banyuasin'),
('997561', '17', '09', 'Kab. Penajam Paser Utara'),
('997578', '24', '15', 'Kab. Rote Ndao'),
('997582', '19', '09', 'Kab. Parigi Moutong'),
('997599', '33', '04', 'Kab. Mamasa'),
('997604', '20', '12', 'Kota Palopo'),
('997611', '03', '16', 'Kota Pariaman'),
('997625', '23', '08', 'Kota Bima'),
('998126', '10', '25', 'Kota Banjar'),
('998130', '02', '22', 'Kab. Nias Selatan'),
('998147', '02', '21', 'Kab. Pakpak Bharat'),
('998151', '02', '23', 'Kab. Humbang Hasundutan'),
('998168', '29', '06', 'Kab. Bangka Barat'),
('998172', '29', '05', 'Kab. Bangka Tengah'),
('998189', '29', '04', 'Kab. Bangka Selatan'),
('998193', '29', '07', 'Kab. Belitung Timur'),
('998201', '07', '06', 'Kab. Seluma'),
('998215', '07', '05', 'Kab. Kaur'),
('998222', '16', '13', 'Kab. Tanah Bumbu'),
('998236', '16', '12', 'Kab. Balangan'),
('998240', '18', '07', 'Kab. Minahasa Selatan'),
('998257', '18', '08', 'Kota Tomohon'),
('998261', '30', '04', 'Kab. Pohuwato'),
('998278', '30', '05', 'Kab. Bone Bolango'),
('998299', '33', '05', 'Kab. Mamuju Utara'),
('998304', '21', '07', 'Kab. Konawe Selatan'),
('998311', '27', '06', 'Kab. Halmahera Utara'),
('998325', '27', '05', 'Kab. Halmahera Selatan'),
('998332', '27', '07', 'Kab. Kepulauan Sula'),
('998346', '27', '04', 'Kab. Halmahera Timur'),
('998350', '27', '08', 'Kota Tidore Kepulauan'),
('998367', '26', '17', 'Kab. Mappi'),
('998371', '26', '18', 'Kab. Asmat'),
('998388', '26', '16', 'Kab. Boven Digoel'),
('998392', '26', '11', 'Kab. Sarmi'),
('998400', '26', '12', 'Kab. Keerom'),
('998414', '26', '15', 'Kab. Tolikara'),
('998421', '26', '14', 'Kab. Pegunungan Bintang'),
('998435', '32', '09', 'Kab. Kaimana'),
('998442', '32', '06', 'Kab. Sorong Selatan'),
('998456', '32', '05', 'Kab. Raja Ampat'),
('998460', '26', '19', 'Kab. Waropen'),
('998477', '26', '13', 'Kab. Yahukimo'),
('998481', '32', '07', 'Kab. Teluk Bintuni'),
('998498', '32', '08', 'Kab. Teluk Wondama'),
('998503', '07', '07', 'Kab. Mukomuko'),
('998510', '24', '16', 'Kab. Manggarai Barat'),
('998840', '31', '00', 'Provinsi Kepulauan Riau'),
('998854', '32', '00', 'Provinsi Papua Barat'),
('999001', '34', '00', 'Provinsi Kalimantan Utara'),
('999002', '10', '27', 'Kab. Pangandaran'),
('999003', '08', '15', 'Kab. Pesisir Barat'),
('999004', '32', '12', 'Kab. Manokwari Selatan'),
('999005', '32', '13', 'Kab. Pegunungan Arfak'),
('999006', '17', '10', 'Kab. Mahakam Ulu'),
('999007', '24', '22', 'Kab. Malaka'),
('999008', '33', '06', 'Kab. Mamuju Tengah'),
('999009', '19', '12', 'Kab. Banggai Laut'),
('999010', '27', '10', 'Kab. Pulau Taliabu'),
('999011', '06', '16', 'Kab. Penukal Abab Lematang Ilir'),
('999012', '21', '14', 'Kab. Kolaka Timur'),
('999013', '19', '13', 'Kab. Morowali Utara'),
('999014', '21', '13', 'Kab. Konawe Kepulauan'),
('999015', '06', '17', 'Kab. Musi Rawas Utara'),
('999016', '21', '16', 'Kab. Buton Tengah'),
('999017', '21', '17', 'Kab. Buton Selatan'),
('999915', '21', '15', 'Kab. Muna Barat');

-- --------------------------------------------------------

--
-- Table structure for table `tprov`
--

CREATE TABLE `tprov` (
  `kdprov` char(2) CHARACTER SET utf8 NOT NULL COMMENT 'kode provinsi',
  `urprov` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'uraian prov/kab/kota'
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

--
-- Dumping data for table `tprov`
--

INSERT INTO `tprov` (`kdprov`, `urprov`) VALUES
('00', 'Nasional'),
('01', 'Provinsi Aceh'),
('02', 'Provinsi Sumatera Utara'),
('03', 'Provinsi Sumatera Barat'),
('04', 'Provinsi Riau'),
('05', 'Provinsi Jambi'),
('06', 'Provinsi Sumatera Selatan'),
('07', 'Provinsi  Bengkulu'),
('08', 'Provinsi  Lampung'),
('09', 'Provinsi DKI Jakarta'),
('10', 'Provinsi Jawa Barat'),
('11', 'Provinsi Jawa Tengah'),
('12', 'Provinsi DI Yogyakarta'),
('13', 'Provinsi Jawa Timur'),
('14', 'Provinsi Kalimantan Barat'),
('15', 'Provinsi Kalimantan Tengah'),
('16', 'Provinsi Kalimantan Selatan'),
('17', 'Provinsi Kalimantan Timur'),
('18', 'Provinsi Sulawesi Utara'),
('19', 'Provinsi Sulawesi Tengah'),
('20', 'Provinsi Sulawesi Selatan'),
('21', 'Provinsi Sulawesi Tenggara'),
('22', 'Provinsi Bali'),
('23', 'Provinsi Nusa Tenggara Barat'),
('24', 'Provinsi Nusa Tenggara Timur'),
('25', 'Provinsi Maluku'),
('26', 'Provinsi Papua'),
('27', 'Provinsi Maluku Utara'),
('28', 'Provinsi Banten'),
('29', 'Provinsi  Bangka Belitung'),
('30', 'Provinsi Gorontalo'),
('31', 'Provinsi Kepulauan Riau'),
('32', 'Provinsi Papua Barat'),
('33', 'Provinsi Sulawesi Barat'),
('34', 'Provinsi Kalimantan Utara'),
('99', 'Jawa Barat');

-- --------------------------------------------------------

--
-- Table structure for table `tuser`
--

CREATE TABLE `tuser` (
  `id_user` int(11) NOT NULL COMMENT 'ID tuser',
  `username` varchar(50) NOT NULL COMMENT 'nama email pemakai',
  `full_name` varchar(50) NOT NULL COMMENT 'nama ditampilkan',
  `password` varchar(100) NOT NULL COMMENT 'password',
  `idlevel` int(11) NOT NULL DEFAULT '3' COMMENT 'level pemakai',
  `aktif` tinyint(1) DEFAULT NULL COMMENT 'status user aktif',
  `tgcreate` datetime DEFAULT NULL COMMENT 'waktu pembuatan',
  `tgupdate` datetime DEFAULT NULL COMMENT 'waktu ubah',
  `tgaktif` datetime DEFAULT NULL COMMENT 'waktu aktif',
  `kdsatker` char(6) DEFAULT NULL COMMENT 'kdsatker pemda',
  `notelp` varchar(50) DEFAULT NULL COMMENT 'nomor telepon',
  `nohp` varchar(50) DEFAULT NULL COMMENT 'nomor hp',
  `jabatan` varchar(50) DEFAULT NULL COMMENT 'nama jabatan'
) ENGINE=InnoDB DEFAULT CHARSET=ascii COMMENT='Tabel user';

--
-- Dumping data for table `tuser`
--

INSERT INTO `tuser` (`id_user`, `username`, `full_name`, `password`, `idlevel`, `aktif`, `tgcreate`, `tgupdate`, `tgaktif`, `kdsatker`, `notelp`, `nohp`, `jabatan`) VALUES
(1, 'super@email.com', 'Admin', '$2a$10$7kSpPN/HbqNQVeu/2nOmkuxYGEIW4J3QjGBMdNXbSXgMHw/1o9TVC', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'oka.kencan@gmail.com', 'Oka Lingga Kencana', '$2a$10$zbzsLsBXqPXXhIBBw8Lo7uAPk7uASVm9FtorgwNPHbXP5YzNfzzZ2', 1, 1, NULL, NULL, NULL, '990828', '0928277718', '099282828', 'kepala');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tlevel`
--
ALTER TABLE `tlevel`
  ADD PRIMARY KEY (`idleveluser`);

--
-- Indexes for table `tpemda`
--
ALTER TABLE `tpemda`
  ADD PRIMARY KEY (`kdsatker`),
  ADD KEY `FK_tpemda_tprov` (`kdprov`) USING BTREE,
  ADD KEY `idx_kdprov` (`kdprov`) USING BTREE;

--
-- Indexes for table `tprov`
--
ALTER TABLE `tprov`
  ADD PRIMARY KEY (`kdprov`);

--
-- Indexes for table `tuser`
--
ALTER TABLE `tuser`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `nmuser` (`username`) USING BTREE,
  ADD KEY `FK_tuser_tuserlevel` (`idlevel`) USING BTREE,
  ADD KEY `FK_tuser_tpemda` (`kdsatker`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tlevel`
--
ALTER TABLE `tlevel`
  MODIFY `idleveluser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID tleveluser', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tuser`
--
ALTER TABLE `tuser`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID tuser', AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tpemda`
--
ALTER TABLE `tpemda`
  ADD CONSTRAINT `tpemda_ibfk_1` FOREIGN KEY (`kdprov`) REFERENCES `tprov` (`kdprov`);

--
-- Constraints for table `tuser`
--
ALTER TABLE `tuser`
  ADD CONSTRAINT `tuser_ibfk_1` FOREIGN KEY (`idlevel`) REFERENCES `tlevel` (`idleveluser`),
  ADD CONSTRAINT `tuser_ibfk_2` FOREIGN KEY (`kdsatker`) REFERENCES `tpemda` (`kdsatker`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
